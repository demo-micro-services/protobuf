# protoc -I. -I$GOPATH/src --go_out=. *.proto
protoc -I. \
    -I$GOPATH/src \
    -I$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
    -I$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway \
    -I ${GOPATH}/src/github.com/envoyproxy/protoc-gen-validate \
    --go_out=plugins=grpc:. \
    --grpc-gateway_out=logtostderr=true:. \
    --swagger_out=logtostderr=true:. \
    *.proto &&
mv *.json ../../../api-gateway/swagger/mid-demo.swagger.json
